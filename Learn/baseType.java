//https://www.cnblogs.com/figozhg/archive/2015/12/29/5087383.html

//变量的声明和使用
fun main(args:Array<String>){
    var yexiao ="又香又好吃的肉包"//一块字符串的空间，空间别名是夜宵,里面存放的内容是又香又好吃的肉包   
    yexiao ="结果发现是一碗红烧肉"
    println("冰箱里存放的:"+yexiao)
}

//容器
//Byte -128~127  8块bit 0000 0001
//Short 16 bit
//Float 小数点精确到6位
//Int 32个bit
//Long 64个bit
 //String ""引起来的都是字符串


//编译器推断 加了:数据类型 就是显式指定
//val常量 var变量 成员变量等区域后续介绍
fun main(args:Array<String>){
    var i:Int =18
    i =19  //Byte =127
    var k:Long =99999999999999
    var s:String ="hah"
    //var a:String =hellp 需要加""
    println(s)
    //智能数据类型根据等号后面推断。
    
    //不可变,val =value只读，不可被重新赋值
    val number ="no.123456"
     
}

//基本的变量取值范围
fun main(args:Array<String>){
    val aByte:Byte=Byte.MAX_VALUE
    val bByte:Byte=Byte.MIN_VALUE
    println("Byte最大值"+aByte)  
    //println(aByte+"Byte最大值") 会抛错在连接那边+号
    println(bByte)

    val aLong:Long=Long.MAX_VALUE
    var bLong:Long =Long.MIN_VALUE
    println(aLong)
    println(bLong)

    val aShort:Short=Short.MAX_VALUE
    var bShort:Short =Short.MIN_VALUE
    println(aShort)
    println(bShort)
     
    val aFloat:Float=Float.MAX_VALUE
    var bFloat:Float =Float.MIN_VALUE
    println(aFloat)
    println(bFloat)
    
    var aInt:Int =0b0011
    println("十进制"+aInt)
    
}